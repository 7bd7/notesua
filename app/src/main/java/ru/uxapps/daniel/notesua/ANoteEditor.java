package ru.uxapps.daniel.notesua;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import ru.uxapps.daniel.notesua.data.NoteContract.NoteEntry;
import ru.uxapps.daniel.notesua.data.NoteDbHelper;


public class ANoteEditor extends AppCompatActivity {

    EditText mTitleEditText;
    EditText mTextEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anote_editor);

        mTitleEditText = (EditText) findViewById(R.id.edit_note_title);
        mTextEditText = (EditText) findViewById(R.id.edit_note_text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    private void insertNote(){
        //it saves note and gets user to main screen
        String titleString = mTitleEditText.getText().toString();
        String textString = mTextEditText.getText().toString();

        NoteDbHelper noteDbHelper = new NoteDbHelper(this);
        SQLiteDatabase db = noteDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NoteEntry.COLUMN_NOTE_TITLE, titleString);
        values.put(NoteEntry.COLUMN_NOTE_TEXT, textString);
        long newRowId = db.insert(NoteEntry.TABLE_NAME, null, values);

        if(newRowId == -1) {
            Toast.makeText(ANoteEditor.this, "Error with saving note", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(ANoteEditor.this, "Note saved with row id: " + newRowId, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete_note:
                // Do nothing for now
                Toast.makeText(ANoteEditor.this, "All note will be deleted...\n...one day", Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_save_note:
                // Save note to db
                insertNote();
                //Toast.makeText(ANoteEditor.this, "Note is saved", Toast.LENGTH_LONG).show();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
