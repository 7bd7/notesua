package ru.uxapps.daniel.notesua.data;


import android.provider.BaseColumns;

public final class NoteContract {

    public static final class   NoteEntry implements BaseColumns{

        public final static String TABLE_NAME = "notes";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_NOTE_TITLE = "title";
        public final static String COLUMN_NOTE_TEXT = "text";

    }


}
