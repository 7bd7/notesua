package ru.uxapps.daniel.notesua;


public class Note {
    private String mText;
    private String mTitle;

    public Note (String title, String text) {
        mText = text;
        mTitle = title;
    }

    public String getText() {
        return mText;
    }

    public String getTitle() {
        return mTitle;
    }
}
