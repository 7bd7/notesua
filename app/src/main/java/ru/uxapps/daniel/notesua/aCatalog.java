package ru.uxapps.daniel.notesua;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.uxapps.daniel.notesua.data.NoteContract.NoteEntry;
import ru.uxapps.daniel.notesua.data.NoteDbHelper;


public class ACatalog extends AppCompatActivity {

    List<Note> notes = new ArrayList<>();
    private NoteDbHelper mNoteDbHelper = new NoteDbHelper(ACatalog.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amain);
        loadData();

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recycler);
        final NotesAdapter adapter = new NotesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClick(Note item) {
                openNote(item);
            }
        });
        rv.setAdapter(adapter);
        adapter.update(notes);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageBitmap(getBitmap(this, R.drawable.add_note));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ACatalog.this, ANoteEditor.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        NotesAdapter notesAdapter = new NotesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClick(Note item) {
            }
        });
        notesAdapter.update(notes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to a click on the "Delete all" menu option
            case R.id.action_delete_all:
                // Do nothing for now
                Toast.makeText(ACatalog.this, "All notes will be deleted...\n...one day", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class NotesAdapter extends RecyclerView.Adapter<NoteVh> {

        private final OnItemClickListener listener;

        private NotesAdapter (OnItemClickListener listener){
            this.listener = listener;
        }

        List<Note> mData = Collections.EMPTY_LIST;

        void update(List<Note> data) {
            mData = data;
        }

        @Override
        public NoteVh onCreateViewHolder(ViewGroup parent, int viewType) {
            return new NoteVh(parent);
        }

        @Override public void onBindViewHolder(NoteVh holder, int position) {
            holder.bind(mData.get(position), listener);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    private static class NoteVh extends RecyclerView.ViewHolder {

        private final TextView mTitle;
        private final TextView mText;

        public NoteVh(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate((R.layout.i_note), parent, false));
            mTitle = (TextView) itemView.findViewById(R.id.i_note_title);
            mText = (TextView) itemView.findViewById(R.id.i_note_text);
        }

        public void bind(final Note note, final OnItemClickListener listener) {
            mTitle.setText(note.getTitle());
            mText.setText(note.getText());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(note);
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    private void loadData() {

        SQLiteDatabase db = mNoteDbHelper.getReadableDatabase();
        String[] projection = {
                NoteEntry._ID,
                NoteEntry.COLUMN_NOTE_TITLE,
                NoteEntry.COLUMN_NOTE_TEXT
        };

        Cursor cursor = db.query(NoteEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);
        int titleColumnIndex = cursor.getColumnIndex(NoteEntry.COLUMN_NOTE_TITLE);
        int textColumnIndex = cursor.getColumnIndex(NoteEntry.COLUMN_NOTE_TEXT);

        notes.clear();
        //// TODO: 11/30/2016 FIX notes.clear() 

        for (int i = 0; cursor.moveToPosition(i); i++) {
            cursor.moveToPosition(i);
            String title = cursor.getString(titleColumnIndex);
            String text = cursor.getString(textColumnIndex);

            notes.add(new Note(title, text));
        }
        cursor.close();
    }

    public interface OnItemClickListener {
        void onItemClick(Note item);
    }

    private void openNote(Note item) {
        Intent intent = new Intent(ACatalog.this, ANoteEditor.class);
        startActivity(intent);
    }
}
